function AddComponent({ add }) {
    const container = document.createElement("div");

    const textInput = document.createElement("input");
    textInput.type = "text";
    container.appendChild(textInput);

    const addButton = document.createElement("input");
    addButton.type = "button";
    addButton.value = "add";
    addButton.addEventListener("click", () => {
        if (textInput.value !== "") {
            add({ toDo: TodoModel.ToDo({ checked: false, text: textInput.value }) });
        }
    });
    container.appendChild(addButton);

    return container;
}

function ToDoComponent({ toDo, check, uncheck, remove }) {
    const container = document.createElement("div");

    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.checked = toDo.checked;
    container.appendChild(checkbox);

    const text = document.createTextNode(toDo.text);
    container.appendChild(text);

    if (toDo.checked) {
        const deleteToDoComponentButton = document.createElement("input");
        deleteToDoComponentButton.type = "button";
        deleteToDoComponentButton.value = "delete";
        deleteToDoComponentButton.style = "color:black"
        deleteToDoComponentButton.style = "background-color:red"
        deleteToDoComponentButton.addEventListener("click", () => {
            remove();
        });
        container.appendChild(deleteToDoComponentButton);
    }

    checkbox.addEventListener("change", event => {
        switch (toDo.checked) {
            case true:
                uncheck();
                break;
            case false:
                check();
                break;
        }
    });
    return container;
}

function ToDoListComponent({ toDoList, removeToDo, checkToDo, uncheckToDo, }) {
    const container = document.createElement("div");

    toDoList
        .map((toDo, index) => {
            return ToDoComponent({
                toDo: toDo,
                check: () => checkToDo({ index }),
                uncheck: () => uncheckToDo({ index }),
                remove: () => removeToDo({ index }),
            });
        })
        .forEach(element => container.appendChild(element));

    return container;
}

function ToDosComponent({ toDos, addToDo, checkToDo, uncheckToDo, removeToDo, }) {
    const container = document.createElement("div");
    container.appendChild(AddComponent({ add: addToDo }));
    container.appendChild(ToDoListComponent({
        toDoList: toDos.toDoList,
        removeToDo,
        checkToDo,
        uncheckToDo,
    })
    );

    return container;
}

function ToDoApp({ initialState }) {
    let currentState = initialState;

    function addToDo({ toDo }) {
        const newState = TodoModel.addToDo({ toDos: currentState.domain, toDo });
        updateDomainState({ newState });
    }

    function removeToDo({ index }) {
        const newState = TodoModel.deleteToDo({ toDos: currentState.domain, index });
        updateDomainState({ newState });
    }

    function checkToDo({ index }) {
        const newState = TodoModel.checkToDoInToDos({ toDos: currentState.domain, index });
        updateDomainState({ newState });
    }

    function uncheckToDo({ index }) {
        const newState = TodoModel.uncheckToDoInToDos({ toDos: currentState.domain, index });
        updateDomainState({ newState });
    }

    const container = document.createElement("div");

    function updateState({ newState }) {
        const newView = ToDosComponent({
            toDos: newState.domain,
            addToDo,
            checkToDo,
            uncheckToDo,
            removeToDo,
        });

        container.innerHTML = "";
        container.appendChild(newView);
        currentState = newState;
    }
    function updateDomainState({ newState }) {
        updateState({ newState: { domain: newState, view: currentState.view } });
    }

    updateState({ newState: initialState });
    return container;
}

document.body.appendChild(
    ToDoApp({
        initialState:
        {
            domain: TodoModel.ToDos({ toDoList: [] }),
            view: { loading: false }
        }
    })
);